package com.company; //Найти определитель матрицы 2х2, 3х3
import java.util.Scanner;
public class hw25_09_3 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Введите размерность матрицы: ");
        int n  = sc.nextInt();
        int m = sc.nextInt();
        if (n != m){
            System.out.println("Matrix isn't square, it will not work :(");
            return;
        }
        System.out.println("Введите элементы матрицы: ");
        int[][] array = new int[n][m];
        for (int i = 0; i < n; i++)
        for (int j = 0; j < m; j++){
            array[i][j] = sc.nextInt();
        }
        System.out.println("Your matrix is: ");
        for (int i = 0; i<n; i++) {
            for (int j = 0; j < m; j++)
                System.out.print(array[i][j] + " ");
            System.out.println();}
        // ввели матрицу

        float opr = (array[0][0] * array[1][1]) - (array[0][1] * array[1][0]);
        System.out.println("Определитель матрицы 2х2: " + opr);


        System.out.println("Lets find matrix determinant 3x3. Write count of rows and colums: ");
        int a1 = sc.nextInt();
        int a2 = sc.nextInt();
        if (a1 != a2){
            System.out.println("Matrix isn't square, it will not work :(");
            return;
        }
        int[][] array2 = new int[a1][a2];
        System.out.println("Write elements of matrix: ");
        for (int i = 0; i < a1; i++)
            for (int j = 0; j < a2; j++)
                array2[i][j] = sc.nextInt();
        System.out.println("Our matrix: ");
            for (int i = 0; i < a1; i++) {
            for (int j = 0; j < a2; j++) {
                System.out.print(array2[i][j] + " ");
            }
            System.out.println();
        }
        float det = (array2[0][0]*array2[1][1]*array2[2][2]+array2[0][2]*array2[1][0]*array2[2][1]+array2[0][1]*array2[1][2]*array2[2][0])-array2[0][2]*array2[1][1]*array2[2][0]-array2[0][1]*array2[1][0]*array2[2][2]-array2[0][0]*array2[1][2]*array2[2][1];
        System.out.println(" Our determinant: " + det);
    }
}
