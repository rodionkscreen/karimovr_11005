package com.company; //Задание на перемножение матриц (сдано)

import java.util.Scanner;
class hw25_09
{
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Введите размер матрицы A: ");
        int a1 = sc.nextInt();
        int a2 = sc.nextInt();
        int[][] A = new int[a1][a2];
        System.out.println("Введите размер матрицы B: ");
        int b1 = sc.nextInt();
        int b2 = sc.nextInt();
        int[][] B = new int[b1][b2];
        { if (a2 != b1)
                System.out.println(a2 + " не равно " + b1 + " !"); }//Проверка на возможность перемножения
        int[][] C = new int[a1][b2];

        System.out.println("Введите элементы матрицы A:");
        for (int i = 0; i<a1; i++) {
            for (int j = 0; j < a2; j++) {
                A[i][j] = sc.nextInt();
            }
        } //Ввод матрицы A
        System.out.println("Введите элементы матрицы B:");
        for (int i = 0; i<b1; i++) {
            for (int j = 0; j < b2; j++) {
                B[i][j] = sc.nextInt();
            }
        } //Ввод матрицы B
        System.out.println("Матрица A: ");
        for (int i = 0; i<a1; i++) {
            for (int j = 0; j < a2; j++) {
                System.out.print(A[i][j] + " ");
            }
            System.out.println(); //Вывод матрицы A
        }

        System.out.println("Матрица B: ");
        for (int i = 0; i<b1; i++) {
            for (int j = 0; j < b2; j++) {
                System.out.print(B[i][j] + " ");
            }
            System.out.println();
        } //Вывод матрицы B

        for (int i = 0; i < a1; i++)
            for (int j = 0; j < b2; j++)
                for (int g = 0; g < a2 ; g++ )
                    C[i][j] += A[i][g]*B[g][j];

         System.out.println("Матрица C: ");
        for (int i = 0; i<a1; i++) {
            for (int j = 0; j < b2; j++) {
                System.out.print(C[i][j] + " ");
            }
            System.out.println(); //Вывод матрицы C
        }

    }
}