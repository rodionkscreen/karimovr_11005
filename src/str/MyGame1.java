package str;

import java.util.Scanner;

class MyGame1 {

    static class Players {
        private String name1; String name2; Integer hp1; Integer hp2;

        public Players(String name1, Integer hp1, String name2, Integer hp2){
            this.name1 = name1;
            this.hp1 = hp1;
            this.name2 = name2;
            this.hp2 = hp2;
        }
    }



    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Write name of player 1: ");                 // читаю ники игроков
        String name1 = sc.nextLine();
        System.out.println("Write name of player 2: ");
        String name2 = sc.nextLine();

        System.out.println("Write hp of " + name1);                     // читаю хп
        int hp1 = sc.nextInt();
        System.out.println("Write hp of " + name2);
        int hp2 = sc.nextInt();

        Players player = new Players(name1, hp1, name2, hp2); // создал инстанс класса Gamer переменной player

        while (hp1 > 0 || hp2 > 0) {
            int damage1 = 0;                                    // читаем damage1
            while (damage1 >= 10 || damage1 <= 0) {
                System.out.println("Write damage [1;9] from player " + name1 + " to player " + name2 + ":");
                damage1 = sc.nextInt();
            }
            if (Math.random() * 10 + 1 > damage1) {
                System.out.println("You damaged! HP player " + name2 + " was: " + hp2);
                hp2 -= damage1;
                System.out.println("HP player " + name2 + " now: " + hp2);
            } else {
                System.out.println("You nepopal. Now HP player " + name2 + " is: " + hp2);
            }
            if (hp2 <= 0) {
                System.out.println("Player " + name1 + " win! Player " + name2 + " is lose");
                break;
            }

            int damage2 = 0;                                    // читаем damage2
            while (damage2 >= 10 || damage2 <= 0) {
                System.out.println("Write damage [1;9] from player " + name2 + " to player " + name1 + ":");
                damage2 = sc.nextInt();
            }
            if (Math.random() * 10 + 1 > damage2) {
                System.out.println("You damaged! HP player " + name1 + " was: " + hp1);
                hp1 -= damage2;
                System.out.println("HP player " + name1 + " now: " + hp1);
            } else {
                System.out.println("You nepopal. Now HP player " + name1 + " is: " + hp1);
            }
            if (hp1 <= 0) {
                System.out.println("Player " + name2 + " is win! Player " + name1 + " is lose");
                break;
            }
        }
    }
}













