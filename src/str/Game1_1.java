package str;

import java.util.Scanner;

class Players {

    private String name1, name2;
    private Integer hp1, hp2;
    Players(String name1, String name2, Integer hp1, Integer hp2){

    }
//getters of variables
    public String getName1() {
        return name1;
    }

    public String getName2() {
        return name2;
    }

    public Integer getHp1() {
        return hp1;
    }

    public Integer getHp2() {
        return hp2;
    }

    public Integer minusHp2(Integer damage){
        int hp = getHp2() - damage;
        return hp;
    }

    public Integer minusHp1(Integer damage){
        int hp = getHp1() - damage;
        return hp;
    }
}


class Game{
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Write name of player 1 & player 2. Then write hp both of the players: ");
        String name1 = sc.nextLine();
        String name2 = sc.nextLine();
        Integer hp1 = sc.nextInt();
        Integer hp2 = sc.nextInt();

        Players players = new Players(name1, name2, hp1, hp2);

        while (players.getHp1() > 0 || players.getHp1() > 0) {
            System.out.println("The move of the " + players.getName1() + ". Write your damage [1;9] to " + players.getName2() + ": ");
            int damage = 0;
            while (damage < 1 || damage > 9) {
                damage = sc.nextInt();
                if (damage < 1 || damage > 9) {
                    System.out.println("Damage is incorrect, write it correctly.");
                }
            }
            if (Math.random() * 10 + 1 > damage) {
                System.out.println("You damaged! HP of player " + players.getName2() + " before attack: " + players.getHp2());
//                players.getHp2() -= damage;
                players.minusHp2(damage);
                System.out.println("After damage: " + players.getHp2());
            } else {
                System.out.println("You miss. HP " + players.getName2() + " is: " + players.getHp2());
            }


            if (players.getHp2() <= 0){
                System.out.println(players.getName2() + " lose. " + players.getName1() + " is win");
                break;
            }


            System.out.println("The move of the " + players.getName2() + ". Write your damage [1;9] to " + players.getName1() + ": ");
            damage = 0;
            while (damage < 1 || damage > 9) {
                damage = sc.nextInt();
                if (damage > 9 || damage < 1){
                    System.out.println("Damage is incorrect, write it correctly.");
                }
            }
            if (Math.random() * 10 + 1 > damage){
                System.out.println("You damaged! HP of player " + players.getName1() + " before attack: " + players.getHp1());
                players.minusHp1(damage);
                System.out.println("After damage: " + players.getHp1());
            } else {
                System.out.println("You miss. HP " + players.getName1() + " is: " + players.getHp1());
            }
            if (players.getHp1() <= 0){
                System.out.println(players.getName1() + " lose. " + players.getName2() + " is win");
                break;
            }
        }
    }
}