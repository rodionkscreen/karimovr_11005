package str;
import java.util.Scanner;
import java.util.ArrayList;

class Buyer {
    private String name;
    private Integer age;
    private boolean men;
    public Buyer(String name, int age, boolean men){
        this.name = name;
        this.age = age;
        this.men = men;
    }

    public void setName(String name) { this.name = name; }
    public String getName() { return name; }

    public void setAge(Integer age) { this.age = age; }
    public Integer getAge() { return age; }

    public void setMen(Boolean men) { this.men = men; }
    public boolean getMen() { return men; }

//    @Override
//    public void toString(Buyer buyer){
////        super();
//        buyer.toString();
//        System.out.println(buyer);
//    }
}


class Product {
    String name;
    Integer price;
    String manufacturer;
    public Product(String name, Integer price, String manufacturer){
        this.name = name;
        this.price = price;
        this.manufacturer = manufacturer;
    }

    public void setName(String name) { this.name = name; }
    public String getName() { return name; }

    public void setPrice(Integer price) { this.price = price; }
    public Integer getPrice() { return price; }

    public void setManufacturer(String manufacturer) { this.manufacturer = manufacturer; }
    public String getManufacturer() { return manufacturer; }

    public void ShowInfoProduct(String name){
        System.out.println("Info about " + getName());
        System.out.println("Price of " + getName() + ": " + getPrice());
        System.out.println("Manufacturer of " + getName() + ": " + getManufacturer());
    }
}

class Order {
//    String boughtProduct;
//    ArrayList<Order> name = new ArrayList<Order>();

    int id;
    Buyer buyer;
    ArrayList<Product> products = new ArrayList<>();

    public void setBuyer(Buyer buyer){ this.buyer = buyer; }
    public void setProducts(Product product) { products.add(product); }

    public Buyer getBuyer(){return buyer;}
    public ArrayList<Product> getProduct(){return products;}
    public int getId(){return id;}
    public void setId(int id){this.id = id;}


}

class hw20_11 {

    public static void main(String[] args) {
//        ArrayList<Buyer> buyers = new ArrayList<Buyer>(); // создали список buyers для хранения объектов
//        buyers.add(new Buyer("Покупатель1"));
//        buyers.add(new Buyer("Покупатель2"));
//        buyers.add(new Buyer("Покупатель3"));
//
//        buyers.toString();
//        System.out.println(buyers);

        Buyer buyer1 = new Buyer("Володя", 20, true);
        Buyer buyer2 = new Buyer("Данил", 19, true);


//        Product[] product = new Product[5];
        ArrayList<Product> products = new ArrayList<Product>(); // создал список продуктов
        products.add(new Product("Молоко", 55, "Ферма Дисон"));
        products.add(new Product("Хлеб", 35, "Волжский Хлебзавод"));
        products.add(new Product("Греча", 30, "Госпоставки airlines"));

    }

    public static void AddProducts(Order order) {
        Scanner sc = new Scanner(System.in);
        boolean addProduct = true;
        while (addProduct) {
            System.out.println("Новый продукт, стоимость и мануфактура: ");
            String name = sc.nextLine();
            Integer price = sc.nextInt();
            String manufacturer = sc.nextLine();

            Product product = new Product(name, price, manufacturer);
            System.out.println("New profuct? [y/n]");
            String word = sc.nextLine();
            while (!word.equals('y') || !word.equals('n')) {
                System.out.println("Write correct answer: y/n: ");
                word = sc.nextLine();
            }
            if (word.equals('n'))
                addProduct = false;


        }
    }

//    public static void addBuyer() {
//        System.out.println("Name, age, man?");
//        Scanner sc = new Scanner(System.in);
//        String name = sc.nextLine();
//        Integer age = sc.nextInt();
//        boolean man = sc.nextBoolean();
//
//        Buyer buyer = new Buyer(name, age, man);
//        Order order = new Order();
//        order.setBuyer(buyer);
//        order.setId(order.id);
//
//        System.out.println("ID: " + order.id);
//
//        AddProducts(order);
//        orders.add(id);
//        id++;
//
//    }
//
//
//
//    public static void secondRequest(int id){
//        Order order =  orders.get(id-1);
//        addProducts(order);
//    }
//
//    public static void thirdRequest(){
//        for (int i = 0; i < orders.size(); i++) {
//            Order order = orders.get(i);
//            Customer customer = order.getCustomer();
//            System.out.println("========================================");
//            System.out.println("Name: " + customer.getName());
//            System.out.println("Sex:  " + customer.getSex());
//            System.out.println("Age:  " + customer.getAge());
//            System.out.println("----------------------------");
//
//            ArrayList<Product> products = order.getProduct();
//            System.out.println("Order " + order.getId());
//            for (int j = 0; j < products.size(); j++) {
//                System.out.println("Name:         " + products.get(j).getName());
//                System.out.println("Cost:         " + products.get(j).getCost() + " RUB");
//                System.out.println("Manufacturer: " + products.get(j).getManufacturer());
//                if(j!= products.size()-1)
//                    System.out.println("----------------------------");
//            }
//            System.out.println("========================================");
//
//            System.out.println("Enter 'Y'");
//            sc.next();
//        }
//    }
//
//    public static void thirdRequest(int id){
//        Order order = orders.get(id-1);
//        Customer customer = order.getCustomer();
//        System.out.println("========================================");
//        System.out.println("Name: " + customer.getName());
//        System.out.println("Sex:  " + customer.getSex());
//        System.out.println("Age:  " + customer.getAge());
//        System.out.println("----------------------------");
//
//        ArrayList<Product> products = order.getProduct();
//        System.out.println("Order " + order.getId());
//        for (int j = 0; j < products.size(); j++) {
//            System.out.println("Name:         " + products.get(j).getName());
//            System.out.println("Cost:         " + products.get(j).getCost() + " RUB");
//            System.out.println("Manufacturer: " + products.get(j).getManufacturer());
//            if(j!= products.size()-1)
//                System.out.println("----------------------------");
//        }
//        System.out.println("========================================");
//    }

//    public static void main(String[] args) {
//        int userRequest = 1;
//        System.out.println("Hello, now you see 3 function, ");
//        System.out.println("that you can use at this program");
//        System.out.println();
//
//        while(userRequest > 0){
//            System.out.println("-----------------------------------");
//            System.out.println("1.Make an order");
//            System.out.println("2.Add some products into some order");
//            System.out.println("3.The data of order(s)");
//            System.out.println("If you don't want to use the program,");
//            System.out.println("just enter '0'");
//            System.out.println("-----------------------------------");
//            System.out.println();
//
//            System.out.println("Choose the function and enter the number");
//            System.out.println();
//
//            userRequest = sc.nextInt();
//            if(userRequest == 1)
//                firstRequest();
//            else if(userRequest == 2) {
//                System.out.println("Enter id of the order");
//                int id = sc.nextInt();
//                secondRequest(id);
//            }
//            else if(userRequest == 0){
//                System.out.println("Thank you for using our program :)");
//                break;
//            }
//            else {
//                System.out.println("Enter id of the order, otherwise get all data");
//                int id = sc.nextInt();
//                if(id > 0)
//                    thirdRequest(id-1);
//                else
//                    thirdRequest();
//            }
//        }
//    }
//
}