import java.util.Scanner;
import java.util.*;

class hw16_10_2 {
    private static Object hw16_10_2;
    static Scanner in = new Scanner(System.in);


    public static void reverse(String textforreverse) { //метод, печатающий обратную строку
        char[] array = textforreverse.toCharArray();
        String b = "";
        for (int i = array.length - 1; i >= 0; i--) {
            b += array[i];
        }
        System.out.println(b);
    }

    public static int maxofarray() { //метод поиска максимального элемента в массиве
        System.out.println("Количестов элементов массива, для поиска максимального элемента: ");
        int arr = in.nextInt();
        int[] Arr = new int[arr];
        int max = 0;
        System.out.println("Введи элементы: ");
        for (int i = 0; i < Arr.length; i++) {
            Arr[i] = in.nextInt();
            if (Arr[i] > max)
                max = Arr[i];
        }
        System.out.println("Максимальный элемент: " + max);
        return max;
    }

    public static int[] dvaarray() { //метод склеивания двух массивов
        System.out.println("Количество элементов первого и второго массивов: ");
        int a1 = in.nextInt(); //a1 - кол-во элементов первого массива
        int[] A1 = new int[a1]; //A1 - первый массив
        int a2 = in.nextInt(); //a2 - кол-во элементов второго массива
        int[] A2 = new int[a2]; //А2 - второй массив
        for (int i = 0; i < a1; i++)
            A1[i] = in.nextInt();
        for (int i = 0; i < a2; i++)
            A2[i] = in.nextInt();
        int a3 = a1 + a2;
        int[] A3 = new int[a3];
        for (int i = 0; i < a1; i++)
            A3[i] = A1[i];
        for (int i = A1.length; i < A3.length; i++)
            A3[i] = A2[i - A1.length];
        for (int i = 0; i < A3.length; i++)
            System.out.print(A3[i] + " ");
        System.out.println();
//        for (int i = 0; i < A3.length; i++)
//            return A3[i];
        return A3;
    }

    public static int[][] transpon () { //метод, транспонирующий матрицу
        System.out.println("Размерность матрицы, для её транспонирования: ");
        int x1 = in.nextInt();
        int x2 = in.nextInt();
        int[][] X = new int[x1][x2]; //X - начальная матрица
        for (int i = 0; i < x1; i++)
            for (int j = 0; j < x2; j++)
                X[i][j] = in.nextInt();
        System.out.println("Начальная матрица: ");
        for (int i = 0; i < x1; i++){
            for (int j = 0; j<x2; j++){
                System.out.print(X[i][j] + " ");
            }
            System.out.println();
        }
        int[][] Y = new int[x2][x1]; //Y - результат
        for (int i = 0; i < x1; i++)
            for (int j = 0; j < x2; j++) {
                Y[j][i] = X[i][j];
            }
        System.out.println("Конечная матрица: ");
        for (int i = 0; i< x2; i++){
            for (int j = 0; j<x1; j++){
                System.out.print(Y[i][j] + " ");
            }
            System.out.println();
        }
        return Y;
    }

    public static int[][] summamatrix () { //Метод суммы матриц
        System.out.println("Сумма матриц. Введите размерность, и элементы первой матрицы:");
        int x1 = in.nextInt();  int y1 = in.nextInt();
        int[][] maxtrix1 = new int[x1][y1];
        for (int i = 0; i < x1; i++)
            for (int j = 0; j < y1; j++)
                maxtrix1[i][j] = in.nextInt();
        System.out.println("Введите размерность второй матрицы: ");
        int x2 = in.nextInt(); int y2 = in.nextInt();
//        if (x1 != x2 && y1 != y2) {
//            System.out.println("Матрицы не равны, нельзя");
//            break;
//        }
        int[][] maxtrix2 = new int[x2][y2];
        System.out.println("Элементы второй матрицы: ");
        for (int i = 0; i < x2; i++)
            for (int j = 0; j < y2; j++)
                maxtrix2[i][j] = in.nextInt();
        int[][] Z = new int[x1][y1];
        for (int i = 0; i < x1; i++) //Сумма
            for (int j = 0; j < y1; j++) {
                Z[i][j] = maxtrix1[i][j] + maxtrix2[i][j];
            }
        for (int i = 0; i < x1; i++) {
            for (int j = 0; j < y1; j++) {
                System.out.print(Z[i][j] + " ");
            }
            System.out.println();
        }
        return Z;
    }

    public static void main(String[] args) {
        System.out.println("Введите текст для реверса: ");
        String textforrevers = in.nextLine();
        reverse(textforrevers);
        maxofarray();
        dvaarray();
        System.out.println(transpon());
        summamatrix();
    }


}