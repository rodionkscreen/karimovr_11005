import java.util.Scanner;

public class hw27_11 {

}

class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        Rectangle rec = new Rectangle(0, 0);
        Circle cir = new Circle(0);
        Triangle tri = new Triangle(0,0);
        System.out.print("Hello, user \n" +
                "Enter a number of figures\n");
        int n = sc.nextInt();
        Shape[] figures = new Shape[n];

        if(n<1){
            System.out.println("Oh, it's impossible\n" +
                    "Thank you for using our program :D");
            return;
        }

        System.out.println("==================================\n" +
                "There are figures available to you:\n" +
                "1.Rectangle\n" +
                "2.Circle\n" +
                "3.Triangle\n" +
                "==================================");



        for (int i = 0; i < n; i++) {
            System.out.println("Choose the figure which you want to add\n" +
                    "and enter a number of the figure without spaces\n" +
                    "(Rectangle/Circle/Triangle)");
            String answer = sc.next();
            do{
                if(answer.equals("1")){
                    System.out.println("Enter length and width of Rectangle (a & b)");
                    figures[i] = new Rectangle(sc.nextDouble(), sc.nextDouble());
                    break;
                }
                else if(answer.equals("2")){
                    System.out.println("Enter radius of Circle (r)");
                    figures[i] = new Circle(sc.nextDouble());
                    break;
                }
                else if(answer.equals("3")){
                    System.out.println("Enter blase length and height of Triangle");
                    figures[i] = new Triangle(sc.nextDouble(), sc.nextDouble());
                    break;
                }
                else
                    System.out.println("Please, enter the valid number");
                answer = sc.next();
            }while(true);
            figures[i].getArea();
        }



        System.out.println("\n" +
                "Areas of figures:");
        for (int i = 0; i < n; i++) {
            if(figures[i].getClass().equals(rec.getClass()))
                System.out.println("Rectangle: " + figures[i].area);
            else if(figures[i].getClass().equals(cir))
                System.out.println("Circle: " + figures[i].area);
            else
                System.out.println("Triangle: " + figures[i].area);
        }

    }
}


class Rectangle extends Shape {
    private double a; // длина прямоугольника
    private double b; // ширина прямоугольника
    private String name = "Rectangle";

    public Rectangle(double a, double b){
        this.a = a;
        this.b = b;
    }
    @Override
    public void getArea() {
        area = a*b;
    }


}

abstract class Shape {
    protected double area;
    public abstract void getArea();
}


class Test {
    public static void main(String[] args) {
        Shape shape = new Circle(4);
        System.out.println(shape.getClass());
    }
}


class Triangle extends Shape {
    private double a; // длина основания
    private double h; // длина высоты, проведенной к основанию

    public Triangle(double a, double h){
        this.a = a;
        this.h = h;
    }

    @Override
    public void getArea(){
        area = a*h/2;
    }
}


class Circle extends Shape {
    private double r; // радиус окружности

    public Circle(double r){
        this.r = r;
    }

    @Override
    public void getArea(){
        area = 3.14 * r;
    }
}