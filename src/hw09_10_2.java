import java.util.Scanner; //Сортировка строки по алфавиту

public class hw09_10_2 {
    public static int countofwords (String text){
        int c = 0;
        for (int i = 0; i < text.length(); i++) {
            while (text.charAt(i) == ' '){
                i++;
                if (i==text.length())
                    break;
            }
            while (text.charAt(i) != ' '){
                i++;
                if (i == text.length()){
                    c++;
                    break;
                }
                if (text.charAt(i) == ' ')
                    c++;
            }
        }
        return c;
    }
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.println("Введите строку: ");
        in.nextLine();
        String text = in.nextLine();
        String[] words=new String[countofwords(text)];
        int l = 0;
        String text1="";
        for (int i = 0; i<text.length(); i++){
            while (text.charAt(i) == ' '){
                i++;
                if (i == text.length())
                    break;
            }
            while (text.charAt(i) !=' '){
                text1 += text.charAt(i);
                i++;
                if (i == text.length() &&  text1 !="")
                    words[l]=text1;
                if (i == text.length())
                    break;
                if (text.charAt(i) ==' '){
                    words[l] = text1;
                    l++;
                    text1="";
                }

            }
        }
        for (int i = 0; i<words.length; i++){
            if(i == words.length-1)
                break;
            for (int j = i+1; j<words.length; j++){
                for (int k = 0; k<words[i].length(); k++){
                    if (words[i].charAt(k)<words[j].charAt(k))
                        break;
                    else if (words[i].charAt(k) == words[j].charAt(k))
                        continue;
                    else {
                        String t = words[i];
                        words[i] = words [j];
                        words [j] = t;
                        break;
                    }
                }
            }
        }
        for (int i = 0; i < words.length; i++)
            System.out.println(words[i] + " ");
    }
}