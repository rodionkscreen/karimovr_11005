import java.util.Scanner;   //есть ли в квадратичном массиве размерностью n, n/2 четных чисел, расположенных по диагонали
public class hw09_10_3 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.println("Size 'Q' matrix: ");
        int q=in.nextInt();
        int[][] Q = new int[q][q];
        System.out.println("Elements of 'Q' matrix: ");
        for (int i = 0; i<q; i++)
            for (int j = 0; j<q; j++)
                Q[i][j] = in.nextInt();
        int x = 0;   //counter of 4ETHbIX
        int[][] result = new int[q][q];
        for (int i = 0; i < q; i++) {
            if (Q[i][i] % 2 == 0){
                x++;
                result[i][i] = 1;
            }
            if (Q[q-i-1][i] % 2 == 0 && result[q-i-1][i] != 1){
                x++;
                result[q-i-1][i] = 1;
            }
        }
//        for (int i = q; i > 0; i--)
//            for (int j = 0; j < q; j++)
//                if (Q[i][j] % 2 == 0)
//                    x++;
        if (x == q/2)
        System.out.println("Yes");
        else System.out.println("No");
    }
}
