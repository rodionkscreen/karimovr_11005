//static - один на все инстансы
// instance - Ваня и Никита.
// Vector2D b = new Vector2D(); - создание instance для переменной, c названием b

import java.util.Scanner;

class Vector2D{
    Scanner sc = new Scanner(System.in);
    double x;
    double y;
    public Vector2D(){
        x = sc.nextDouble();
        y = sc.nextDouble();
    }

    public void add(Vector2D other){
        System.out.println("Задание 1: ");
        System.out.println(x);
        System.out.println(y);
        x = x + other.x;
        y = y + other.y;
        System.out.println(x);
        System.out.println(y);
        System.out.println();
    }

    public void sub(Vector2D other){
        System.out.println("Задание 2: ");
        System.out.println(x);
        System.out.println(y);
        x = x - other.x;
        y = y - other.y;
        System.out.println(x);
        System.out.println(y);
        System.out.println();
    }

    public void mult(double b){
        System.out.println("Задание 3: ");
        System.out.println(x);
        System.out.println(y);
        x = x * b;
        y = y * b;
        System.out.println(x);
        System.out.println(y);
    }

    public static void main(String[] args) {
        Vector2D a = new Vector2D(); // создал instance a
        Vector2D b = new Vector2D(); // cоздал instance b
        a.add(b);
        a.sub(b);
        int q = 2;
        a.mult(q);
    }
}

