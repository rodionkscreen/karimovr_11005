import javax.swing.JButton;
import javax.swing.JFrame; // or import javax.swing.*
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.JList;
import javax.swing.*;
import java.awt.*;
// First Calculator:
class Calculator {
    public static void main(String[] args) {

//    JButton jButton = new JButton();
//    JFrame jFrame = new JFrame();
//    JPanel jPanel = new JPanel();

        JPanel windowContent = new JPanel(); // create panel

        FlowLayout f1 = new FlowLayout(); // create manager "out" for this panel
        windowContent.setLayout(f1);

        JLabel label1 = new JLabel("Number 1:"); // create components in memory
        JTextField field1 = new JTextField(10);
        JLabel label2 = new JLabel("Number 2:");
        JTextField field2 = new JTextField(10);
        JLabel label3 = new JLabel("Sum:");
        JTextField result = new JTextField(10);
        JButton go = new JButton("Add");

        windowContent.add(label1);  // add components on panel
        windowContent.add(field1);
        windowContent.add(label2);
        windowContent.add(field2);
        windowContent.add(label3);
        windowContent.add(result);
        windowContent.add(go);

        JFrame frame = new JFrame("My First Calculator");   // create frame and add panel
        frame.setContentPane(windowContent);

        frame.setSize(400, 100); // add size and do frame uninvisible
        frame.setVisible(true);
    }
}

// Second Calculator:
class Calculator2{
    JPanel windowContent;
    JTextField displayField;
    JButton button0;
    JButton button1;
    JButton button2;
    JButton button3;
    JButton button4;
    JButton button5;
    JButton button6;
    JButton button7;
    JButton button8;
    JButton button9;
    JButton buttonPoint;
    JButton buttonEqual;
    JPanel p1;

    Calculator2() {
        windowContent = new JPanel();

        BorderLayout bl = new BorderLayout();
        windowContent.setLayout(bl);

        displayField = new JTextField(30);
        windowContent.add("North", displayField);

        button0 = new JButton("0");
        button1 = new JButton("1");
        button2 = new JButton("2");
        button3 = new JButton("3");
        button4 = new JButton("4");
        button5 = new JButton("5");
        button6 = new JButton("6");
        button7 = new JButton("7");
        button8 = new JButton("8");
        button9 = new JButton("9");
        buttonPoint = new JButton(".");
        buttonEqual = new JButton("=");

        p1 = new JPanel();
        GridLayout gl = new GridLayout(4, 3);
        p1.setLayout(gl);

        p1.add(button1);
        p1.add(button2);
        p1.add(button3);
        p1.add(button4);
        p1.add(button5);
        p1.add(button6);
        p1.add(button7);
        p1.add(button8);
        p1.add(button9);
        p1.add(button0);
        p1.add(buttonPoint);
        p1.add(buttonEqual);

        windowContent.add("Center", p1);

        JFrame frame = new JFrame("Calculator");
        frame.setContentPane(windowContent);

        frame.pack();

        frame.setVisible(true);
    }

    public static void main(String[] args) {
        Calculator calculator = new Calculator();

    }

}