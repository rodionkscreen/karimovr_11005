import com.sun.org.apache.xpath.internal.objects.XString;
import java.util.Scanner;
import java.util.*;

class hw23_10{
    private static Object tests;
    static Scanner in = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.println("1. Рекурсивное произведение чисел от 1 до "); // exercize 1
        int n = in.nextInt();
        System.out.println(repr(n));

        System.out.println("2. Вывести рекурсивно числа от ");   // exercize 2
        int a = in.nextInt();
        int b = in.nextInt();
        out(a,b);
        System.out.println();

        System.out.println("Вычислить число Фиббоначи с номером: "); // exercize 2
        int x = in.nextInt();
        System.out.println(fib(x));
        System.out.println();

        System.out.println("Функция Аккермана для целых m u n: ");
        int m = in.nextInt();
        int n1 = in.nextInt();
        System.out.println(akkerman(m, n1));


    }

    public static int repr(int n) {
        if (n == 0) {
            return 1;
        }
        return n * repr(n-1);
    }
    public static int out(int a, int b){ //описание вывода чисел от A до B рекурсивно
        System.out.print(a + " ");
        a++;
        if (a <= b)
            out(a,b);
        return a;
    }

    public static int x1 = 2; // Номер числа
    public static int prev = 1; // Предыдущее число
    public static int sum = 0; // Вычисленное число
    public static int temp; // Буферная переменная
    public static int fib(int x){ // Как я хочу. Чтобы была какая-то переменная, которая будет счетчиком данного числа, которое мы успели посчитать.
        // Пока этот номер не дойдет до заданного N, будет считаться это число Фибоначи.
        // Пример. Ввели число N. В ф-ции есть новая переменная x1, которая является счетчиком числа, которое мы посчитаем
        //..далее. Пока наше число x1 не равно заданному N, считаем дальше.
        if (x1 != x){
            x1++; // номер числа
            temp = sum;
            sum = prev + sum;// вычисляем число
            prev = temp;
            fib(x);
        }
        return sum;
//            0   1   1   2   3   5   8   13   21   34
//            1.  2.  3.  4.  5.  6.  7.  8.   9.   10.
    }
    public static int akkerman(int m, int n){
        if (m == 0)
            return n + 1;
        if (m > 0 && n == 0){
            return akkerman(m -1, n);
        }
        else{
            return akkerman(m-1, akkerman(m, n - 1));
        }
    }
}