////4 балла:
////Симуляция происходит в бесконечном цикле. Один проход цикла - одна условная единица времени (например, минута).
////Все созданные заказы помещаются в список готовящихся, с таймером. Каждый проход цикла все таймеры снижаются на 1.
////Те блюда, у которых таймер закончился, выдаются покупателю.
////С какой-то вероятностью p1 (вынести в отдельную переменную для настройки) в каждом проходе цикла может добавиться
////новый покупатель. Он сразу делает какой-то случайный заказ.
//
//import java.util.ArrayList;
//import java.util.Scanner;
//
//import static java.util.Arrays.binarySearch;
//
//class Order{
//    String order;
//    int id;
//    int time;
//    int timeLeft;
//    Order (String order){
//        this.order = order;
//        this.id = id;
//    }
//}
//
//class Client{
//    String name;
//    int id;
//    int orderid;
//    int time;
//    Client(String name, int id, int orderid){
//        this.name = name;
//        this.id = id;
//        this.orderid = orderid;
//    }
//}
//
//class hw22_12_1 {
//    public static void main(String[] args) {
//        ArrayList<Order> orders = new ArrayList<>();
//
//        Order order0 = new Order("Гречневая каша");
//        orders.add(order0);
//        int id = binarySearch(orders, 0, 10000000, null);
//        order0.id = id;
//        order0.time = 15;
//
//        Order order1 = new Order("Перловая каша");
//        orders.add(order1);
//        id = binarySearch(orders, 0, 10000000, null);
//        order1.id = id;
//        order1.time = 25;
//
//        Order order2 = new Order("Овсяная каша");
//        orders.add(order2);
//        id = binarySearch(orders, 0, 10000000, null);
//        order2.id = id;
//        order2.time = 15;
//
//
//        Order order3 = new Order("Кукурузная каша");
//        orders.add(order3);
//        id = binarySearch(orders, 0, 10000000, null);
//        order3.id = id;
//        order3.time = 20;
//
//
//        Order order4 = new Order("Манная каша");
//        orders.add(order4);
//        id = binarySearch(orders, 0, 10000000, null);
//        order4.id = id;
//        order4.time = 8;
//
//
//        Order order5 = new Order("Геркулесовая каша");
//        orders.add(order5);
//        id = binarySearch(orders, 0, 10000000, null);
//        order5.id = id;
//        order5.time = 18;
//
//
//        Order order6 = new Order("Уха");
//        orders.add(order6);
//        id = binarySearch(orders, 0, 10000000, null);
//        order6.id = id;
//        order6.time = 35;
//
//
//        Order order7 = new Order("Борщ");
//        orders.add(order7);
//        id = binarySearch(orders, 0, 10000000, null);
//        order7.id = id;
//        order7.time = 40;
//
//
//        Order order8 = new Order("Картофель");
//        orders.add(order8);
//        id = binarySearch(orders, 0, 10000000, null);
//        order8.id = id;
//        order8.time = 10;
//
//
//        Order order9 = new Order("Макароны");
//        orders.add(order9);
//        id = binarySearch(orders, 0, 10000000, null);
//        order9.id = id;
//        order9.time = 12;
//
//
//        Order order10 = new Order("Рис");
//        orders.add(order10);
//        id = binarySearch(orders, 0, 10000000, null);
//        order10.id = id;
//        order10.time = 25;
//
//        // Как можно было сделать по другому?..
//
//        Scanner sc = new Scanner(System.in);
//        boolean start = true;
//        int time = 0;
//        ArrayList<Client> clients = new ArrayList<>();
//        boolean addedclient = false;
//
//        while (start) {
//            time++;
//
//            // Если уже есть клиенты, уменьшается время на таймере
//            if (clients.get(0) != null) {
//                int z = 0;
//                while (clients.get(z) != null) {
//                    clients.get(z).time -= 1;
//                    if (clients.get(z).time == 0)
//                        System.out.println("Заказ клиента " + clients.get(z).name + " готов.");
//                }
//            }
//
//            // Если клиентов - 0
//            if (clients.get(0) == null) {
//                System.out.println("Write name of client (eg Ivan_Berdnik): ");
//                String client_name = sc.nextLine();
//                int orderid = (int) (Math.random() * 10);
//                clients.add(new Client(client_name, 0, orderid));
//                clients.get(0).time = orders.get(orderid).time;
//                System.out.println("New client[0]: " + clients.get(0).name + ". Time to ready: "
//                        + orders.get(orderid).order + " " + clients.get(0).time);
//
//                break;
//            }
//
//            // С какой - то вероятностью добавится новый клиент u случайно делает заказ
//            if (Math.random() * 10 >= 5) {
//                System.out.println("Write name of client (eg Ivan_Berdnik): ");
//                String client_name = sc.nextLine();
//                id = binarySearch2(clients, 0, 10000000, null);
//                int orderid = (int) (Math.random() * 10);
//                clients.add(new Client(client_name, id, orderid));
//                clients.get(id).time = orders.get(orderid).time;
//                System.out.println("New client[" + id + "]" + clients.get(id).name + ". Time to ready: " +
//                        clients.get(id).time);
//
//            }
//        }
//    }
//
//    private static int binarySearch(ArrayList<Order> orders, int i1, int i2, Object o) {
//        int i;
//        for (i = i1; i < i2; i++) {
//            if (orders.get(i) == o)
//                break;
//        }
//        return (i - 1);
//    }
//    private static int binarySearch2(ArrayList<Client> clients, int i1, int i2, Object o) {
//        int i;
//        for (i = i1; i < i2; i++) {
//            if (clients.get(i) == o)
//                break;
//        }
//        return (i - 1);
//    }
//}
