 import java.util.Arrays; //find the longest word

public class hw25_09_2 {
    public static void main(String[] args) {
        String str = "I'am a really programmer";
        String[]  text = str.split(" ");
        System.out.println("The longest word is: " + maxLengthWord(text));
    }

    private static String maxLengthWord(String[] s) {
        for (int i = 0; i < s.length; i++) {
            s[i] = s[i].toLowerCase();
        }
        Arrays.sort(s);
        int len = 0;
        String longest = "";
        for (String string : s) {
            if (string.length() > len) {
                len = string.length();
                longest = string;
            }
        }
        return longest;
    }
}